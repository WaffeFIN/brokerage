package math;

/**
 *
 * @author Walter Grönholm
 */
public class Math {

    private Math() {
    }

    public static int gcd(int a, int b) {
        if (b == 0) {
            return a;
        }
        return gcd(b, a % b);
    }

    public static double median(double a, double b, double c) {
        double x = a - b;
        double y = b - c;
        double z = a - c;
        if (x * y > 0) {
            return b;
        }
        if (x * z > 0) {
            return c;
        }
        return a;
    }
}
