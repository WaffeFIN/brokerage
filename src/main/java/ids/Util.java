package ids;

import java.util.UUID;

/**
 *
 * @author Walter Grönholm
 */
public class Util {

    private Util() {
    }
    
    public static String generateOfferId() {
        return generateTransactionId();
    }
    
    public static String generateTransactionId() {
        return UUID.randomUUID().toString();
    }
}
