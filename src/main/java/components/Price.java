package components;

import data.Currency;
import com.artemis.Component;

/**
 *
 * @author Walter Grönholm
 */
public class Price extends Component {

    public Currency currency;
    public double value;

    public Price() {
    }

    public Price(Currency currency, double value) {
        this.currency = currency;
        this.value = value;
    }

    public Price copy(Price template) {
        this.currency = template.currency;
        this.value = template.value;
        return this;
    }
}
