package components;

import com.artemis.Component;

/**
 *
 * @author Walter Grönholm
 */
public class LoginRequest extends Component {

    public String userName;
    public String password;
}
