package components;

import com.artemis.Component;

/**
 *
 * @author Walter Grönholm
 */
public class TimedDeletion extends Component {

    /**
     * Earliest possible deletion time, in milliseconds.
     */
    public long deletionTime;
}
