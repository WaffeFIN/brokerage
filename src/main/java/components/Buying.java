package components;

import com.artemis.Component;

/**
 *
 * @author Walter Grönholm
 */
public class Buying extends Component {

    public int amount;
    public String itemId;

    public Buying() {
    }

    public Buying(int amount, String itemId) {
        this.amount = amount;
        this.itemId = itemId;
    }

    public Buying copy(Buying template) {
        this.amount = template.amount;
        this.itemId = template.itemId;
        return this;
    }
    
}
