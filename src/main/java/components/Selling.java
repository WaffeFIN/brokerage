package components;

import com.artemis.Component;

/**
 *
 * @author Walter Grönholm
 */
public class Selling extends Component {

    public int amount;
    public String itemId;
    
    public Selling() {
    }

    public Selling(int amount, String itemId) {
        this.amount = amount;
        this.itemId = itemId;
    }

    public Selling copy(Selling template) {
        this.amount = template.amount;
        this.itemId = template.itemId;
        return this;
    }
}
