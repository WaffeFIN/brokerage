package components;

import com.artemis.Component;

/**
 *
 * @author Walter Grönholm
 */
public class LinearPriceDelta extends Component {

    public double maxValue;
    public double minValue;
    public double change;
}
