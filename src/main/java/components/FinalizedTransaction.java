package components;

import com.artemis.Component;

/**
 *
 * @author Walter Grönholm
 */
public class FinalizedTransaction extends Component {

    public String id;
    public String sellingOfferId;
    public String sellerId;
    public String buyingOfferId;
    public String buyerId;
}
