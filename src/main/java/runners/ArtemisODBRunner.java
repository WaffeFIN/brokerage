package runners;

import runners.strategies.ArtemisODBStrategy;
import com.artemis.World;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Walter Grönholm
 */
public class ArtemisODBRunner implements Runnable {

    private static final Logger LOG = LoggerFactory.getLogger(ArtemisODBRunner.class);

    private final ArtemisODBStrategy strategy;
    private final double hz;

    public ArtemisODBRunner(ArtemisODBStrategy strategy, double hz) {
        this.strategy = strategy;
        this.hz = hz;
    }

    @Override
    public void run() {
        World world = new World(strategy.getConfig());
        world.setDelta((float) (1.0 / hz));

        LOG.info("Initialized world, running logic on " + hz + " Hz...");
        while (true) {
            try { //see ThreadPoolExecutor
                Thread.sleep((long) ((1000.0 / hz) - System.currentTimeMillis() % (1000.0 / hz)));
            } catch (InterruptedException ex) {
            }
            strategy.step(world);
        }
    }

}
