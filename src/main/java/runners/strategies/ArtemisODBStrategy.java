package runners.strategies;

import com.artemis.World;
import com.artemis.WorldConfiguration;

/**
 *
 * @author Walter Grönholm
 */
public interface ArtemisODBStrategy {

    WorldConfiguration getConfig();

    void step(World world);
}
