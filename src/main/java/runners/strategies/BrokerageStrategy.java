package runners.strategies;

import com.artemis.World;
import com.artemis.WorldConfiguration;
import com.artemis.WorldConfigurationBuilder;
import systems.*;

/**
 *
 * @author Walter Grönholm
 */
public class BrokerageStrategy implements ArtemisODBStrategy {

    @Override
    public WorldConfiguration getConfig() {
        return new WorldConfigurationBuilder()
            .with(new DemoInitializerSystem())
            .with(new OfferCreationSystem(), new LoginRequestSystem())
            .with(new PurchaseSystem()) //new SimpleTradeSystem()
            .with(new EmptyPurchaseRemoverSystem(), new TimedDeletionSystem())
            .with(new FinalizedTransactionLoggerSystem())
            .with(new SystemOutRenderSystem())
            .build();
    }

    @Override
    public void step(World world) {
        world.process();
    }

}
