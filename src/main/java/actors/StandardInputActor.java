package actors;

import components.Buying;
import components.Price;
import components.Selling;
import data.Currency;
import interfaces.TransactionInterface;
import java.util.Scanner;
import java.util.concurrent.ConcurrentLinkedQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Walter Grönholm
 */
public class StandardInputActor implements Actor {

    private static final Logger LOG = LoggerFactory.getLogger(StandardInputActor.class);

    private final String token;
    private final ConcurrentLinkedQueue<String> commands;

    public StandardInputActor(String token) {
        this.token = token;
        commands = new ConcurrentLinkedQueue<>();

        new Thread() {
            @Override
            public void run() {
                Scanner scanner = new Scanner(System.in);
                while (true) {
                    String command = scanner.nextLine();
                    commands.add(command);
                }
            }
        }.start();
    }

    @Override
    public void actOn(TransactionInterface transactionInterface) {
        while (!commands.isEmpty()) {
            String command = commands.poll();
            try {
                parseAndRunCommand(transactionInterface, command);
            } catch (Exception ex) {
                LOG.warn("Recieved invalid input", ex);
            }
        }
    }

    private void parseAndRunCommand(TransactionInterface transactionInterface, String command) {
        String[] parts = command.split(" ");
        if (parts.length == 5) {
            String action = parts[0].toLowerCase();
            double amount1 = Double.parseDouble(parts[1]);
            String id1 = parts[2];
            double amount2 = Double.parseDouble(parts[3]);
            String id2 = parts[4];

            switch (action) {
                case "b":
                case "buy":
                    buy(transactionInterface, amount1, id1, amount2, id2);
                    break;
                case "s":
                case "sell":
                    sell(transactionInterface, amount1, id1, amount2, id2);
                    break;
                case "t":
                case "trade":
                    trade(transactionInterface, amount1, id1, amount2, id2);
                    break;
            }
        }
    }

    private void buy(TransactionInterface transactionInterface, double amount, String itemId, double money, String currency) {
        Buying buying = new Buying((int) amount, itemId);
        Price price = new Price(Currency.parseCurrency(currency), money);
        transactionInterface.submitOffer(token, buying, price);
    }

    private void sell(TransactionInterface transactionInterface, double amount, String itemId, double money, String currency) {
        Selling selling = new Selling((int) amount, itemId);
        Price price = new Price(Currency.parseCurrency(currency), money);
        transactionInterface.submitOffer(token, selling, price);
    }

    private void trade(TransactionInterface transactionInterface, double amount1, String id1, double amount2, String id2) {
        Buying buying = new Buying((int) amount1, id1);
        Selling selling = new Selling((int) amount2, id2);
        transactionInterface.submitOffer(token, buying, selling);
    }

}
