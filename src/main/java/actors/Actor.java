package actors;

import interfaces.TransactionInterface;

/**
 *
 * @author Walter Grönholm
 */
public interface Actor {

    void actOn(TransactionInterface worldInterface);
}
