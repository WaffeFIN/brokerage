package systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.systems.IntervalIteratingSystem;
import components.TimedDeletion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Walter Grönholm
 */
public class TimedDeletionSystem extends IntervalIteratingSystem {
    
    private static final Logger LOG = LoggerFactory.getLogger(TimedDeletionSystem.class);
    
    private ComponentMapper<TimedDeletion> deletions;
    private long currentTime;

    public TimedDeletionSystem() {
        super(Aspect.all(TimedDeletion.class), 2.0f);
    }

    @Override
    protected void begin() {
        currentTime = System.currentTimeMillis();
    }

    @Override
    protected void process(int i) {
        if (deletions.get(i).deletionTime <= currentTime) {
            LOG.debug("Removing entity " + i);
            getWorld().delete(i);
        }
    }

}
