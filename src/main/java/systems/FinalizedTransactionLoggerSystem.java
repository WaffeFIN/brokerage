package systems;

import com.artemis.Aspect;
import com.artemis.BaseEntitySystem;
import com.artemis.ComponentMapper;
import components.Buying;
import components.FinalizedTransaction;
import components.Price;
import components.Selling;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Walter Grönholm
 */
public class FinalizedTransactionLoggerSystem extends BaseEntitySystem {

    private static final Logger LOG = LoggerFactory.getLogger(FinalizedTransactionLoggerSystem.class);

    private ComponentMapper<Price> prices;
    private ComponentMapper<Selling> sellings;
    private ComponentMapper<Buying> buyings;
    private ComponentMapper<FinalizedTransaction> transactions;

    public FinalizedTransactionLoggerSystem() {
        super(Aspect.all(FinalizedTransaction.class));
    }

    @Override
    protected void processSystem() {
    }

    @Override
    protected void inserted(int i) {
        LOG.info("Transaction finalized: " + stringifyTransaction(i));
    }

    private String stringifyTransaction(int i) {
        StringBuilder builder = new StringBuilder();
        FinalizedTransaction transaction = transactions.get(i);
        builder.append(transaction.id)
            .append("\n\tfrom: ").append(transaction.sellerId).append(" with offer: ").append(transaction.sellingOfferId)
            .append("\n\tto:   ").append(transaction.buyerId).append(" on offer:   ").append(transaction.buyingOfferId)
            .append('\n');
        boolean first = true;
        Buying buying = buyings.get(i);
        if (buying != null) {
            builder.append(" bought ").append(buying.amount).append(' ').append(buying.itemId);
            first = false;
        }
        Selling selling = sellings.get(i);
        if (selling != null) {
            if (!first) {
                builder.append(" and");
            }
            builder.append(" sold ").append(selling.amount).append(' ').append(selling.itemId);
        }
        Price price = prices.get(i);
        if (price != null) {
            builder.append(" for ").append(price.currency).append(price.value);
        }
        return builder.toString();
    }

}
