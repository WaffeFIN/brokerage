package systems;

import com.artemis.Aspect;
import com.artemis.BaseEntitySystem;
import com.artemis.ComponentMapper;
import components.Buying;
import components.FinalizedTransaction;
import components.Price;
import components.Offer;
import components.Selling;
import data.structures.DeepIterator;
import data.structures.Fraction;
import static data.structures.MapUtil.addToMapList;
import static data.structures.MapUtil.getOrCreate;
import static data.structures.MapUtil.removeFromMapList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Walter Grönholm
 */
public class SimpleTradeSystem extends BaseEntitySystem {

    private static final Logger LOG = LoggerFactory.getLogger(PurchaseSystem.class);

    private ComponentMapper<Offer> offers;
    private ComponentMapper<Selling> sellings;
    private ComponentMapper<Buying> buyings;
    private ComponentMapper<FinalizedTransaction> transactions;

    //sold, bought, ratio, entityIds
    private final Map<String, Map<String, TreeMap<Fraction, List<Integer>>>> tradeMarket;

    public SimpleTradeSystem() {
        super(Aspect.all(Offer.class, Selling.class, Buying.class).exclude(Price.class, FinalizedTransaction.class));
        tradeMarket = new HashMap<>();
    }

    @Override
    protected void processSystem() {
    }

    @Override
    protected void inserted(int i) {
        Selling selling = sellings.get(i);
        Buying buying = buyings.get(i);
        NavigableMap<Fraction, List<Integer>> market = getMarket(buying.itemId, selling.itemId);

        market = market.headMap(Fraction.of(selling.amount, buying.amount), true);
        Iterator<Integer> tradeIds = new DeepIterator(market.values());
        while (tradeIds.hasNext() && buying.amount > 0) {
            int tradeId = tradeIds.next();
            Selling tradeSelling = sellings.get(tradeId);
            //TODO
        }
        if (buying.amount > 0) {
            addToTradeMarket(selling, buying, i);
        }
    }

    private TreeMap<Fraction, List<Integer>> getMarket(String marketSellItemId, String marketBuyItemId) {
        return getOrCreate(getOrCreate(tradeMarket, marketSellItemId, () -> new HashMap<>()), marketBuyItemId, () -> new TreeMap<>());
    }

    @Override
    protected void removed(int i) {
        Selling selling = sellings.get(i);
        Buying buying = buyings.get(i);
        removeFromTradeMarket(selling, buying, i);
    }

    private void addToTradeMarket(Selling selling, Buying buying, int i) {
        TreeMap<Fraction, List<Integer>> market = getMarket(selling.itemId, buying.itemId);
        addToMapList(market, Fraction.of(selling.amount, buying.amount), i);
    }

    private void removeFromTradeMarket(Selling selling, Buying buying, int i) {
        TreeMap<Fraction, List<Integer>> market = getMarket(selling.itemId, buying.itemId);
        removeFromMapList(market, Fraction.of(selling.amount, buying.amount), i);
        removeMarketIfEmpty(selling.itemId, buying.itemId);
    }

    private void removeMarketIfEmpty(String marketSellItemId, String marketBuyItemId) {
        Map<String, TreeMap<Fraction, List<Integer>>> buyMap = tradeMarket.get(marketSellItemId);
        TreeMap<Fraction, List<Integer>> market = buyMap.get(marketBuyItemId);
        if (market.isEmpty()) {
            buyMap.remove(marketBuyItemId);
        }
        if (buyMap.isEmpty()) {
            tradeMarket.remove(marketSellItemId);
        }
    }
    
}
