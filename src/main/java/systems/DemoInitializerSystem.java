package systems;

import com.artemis.BaseSystem;
import com.artemis.ComponentMapper;
import components.LoginRequest;

/**
 *
 * @author Walter Grönholm
 */
public class DemoInitializerSystem extends BaseSystem {
    
    private ComponentMapper<LoginRequest> loginRequests;

    @Override
    protected void initialize() {
        int i = getWorld().create();
        LoginRequest loginRequest = loginRequests.create(i);
        loginRequest.userName = "walter";
        loginRequest.password = "1234";
        setEnabled(false);
    }

    @Override
    protected void processSystem() {
    }
    

}
