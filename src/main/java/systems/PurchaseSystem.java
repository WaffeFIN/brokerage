package systems;

import com.artemis.Aspect;
import com.artemis.BaseEntitySystem;
import com.artemis.ComponentMapper;
import components.Buying;
import components.FinalizedTransaction;
import components.Offer;
import components.Price;
import components.Selling;
import data.structures.DeepIterator;
import static data.structures.MapUtil.addToMapList;
import static data.structures.MapUtil.removeFromMapList;
import static ids.Util.generateTransactionId;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Walter Grönholm
 */
public class PurchaseSystem extends BaseEntitySystem {
    
    //Create common system for buy-sell and sell-buy?
    //Combine Price into Buying/Selling?
    //    buying goods selling money

    private static final Logger LOG = LoggerFactory.getLogger(PurchaseSystem.class);

    private ComponentMapper<Price> prices;
    private ComponentMapper<Offer> offers;
    private ComponentMapper<Selling> sellings;
    private ComponentMapper<Buying> buyings;
    private ComponentMapper<FinalizedTransaction> transactions;

    private final Map<String, Market> markets;

    public PurchaseSystem() {
        super(Aspect.all(Price.class, Offer.class).one(Selling.class, Buying.class).exclude(FinalizedTransaction.class));
        markets = new HashMap<>();
    }

    @Override
    protected void processSystem() {
    }

    @Override
    protected void inserted(int entityId) {
        boolean isSelling = sellings.has(entityId);
        String itemId;
        if (isSelling) {
            Selling selling = sellings.get(entityId);
            itemId = selling.itemId;
        } else {
            Buying buying = buyings.get(entityId);
            itemId = buying.itemId;
        }
        if (!markets.containsKey(itemId)) {
            markets.put(itemId, new Market());
        }
        Market market = markets.get(itemId);
        Price price = prices.get(entityId);
        TreeMap<Double, List<Integer>> marketMap;
        if (isSelling) {
            SortedMap<Double, List<Integer>> highestBuy = market.buyingEntityIds.tailMap(price.value, true);
            if (!highestBuy.isEmpty()) {
                if (finalizeTransaction(itemId, entityId, new DeepIterator(highestBuy.values()), price))
                    return;
            }
            LOG.debug("Selling " + itemId + " for " + price.currency + price.value);
            marketMap = market.sellingEntityIds;
        } else {
            SortedMap<Double, List<Integer>> lowestSell = market.sellingEntityIds.headMap(price.value, true);
            if (!lowestSell.isEmpty()) {
                if (finalizeTransaction(itemId, new DeepIterator(lowestSell.values()), entityId, price))
                    return;
            }
            LOG.debug("Buying " + itemId + " for " + price.currency + price.value);
            marketMap = market.buyingEntityIds;
        }
        addToMapList(marketMap, price.value, entityId);
    }

    @Override
    protected void removed(int entityId) {
        boolean isSelling = sellings.has(entityId);
        String itemId;
        if (isSelling) {
            Selling selling = sellings.get(entityId);
            itemId = selling.itemId;
        } else {
            Buying buying = buyings.get(entityId);
            itemId = buying.itemId;
        }
        if (markets.containsKey(itemId)) {
            Market market = markets.get(itemId);
            Price price = prices.get(entityId);
            TreeMap<Double, List<Integer>> marketMap = isSelling ? market.sellingEntityIds : market.buyingEntityIds;
            removeFromMapList(marketMap, price.value, entityId);
        }
    }

    private boolean finalizeTransaction(String itemId, int sellingId, Iterator<Integer> buyingIds, Price price) {
        Selling selling = sellings.get(sellingId);
        while (buyingIds.hasNext()) {
            Integer buyingId = buyingIds.next();
            Buying buying = buyings.get(buyingId);
            if (selling.amount <= 0) {
                break;
            }
            int entityId = getWorld().create();
            FinalizedTransaction transaction = transactions.create(entityId);
            transaction.id = generateTransactionId();
            Offer sellerOffer = offers.get(sellingId);
            transaction.sellingOfferId = sellerOffer.id;
            transaction.sellerId = sellerOffer.userId;
            Offer buyerOffer = offers.get(buyingId);
            transaction.buyingOfferId = buyerOffer.id;
            transaction.buyerId = buyerOffer.userId;
            Selling newSelling = sellings.create(entityId);
            newSelling.amount = Math.min(selling.amount, buying.amount);
            newSelling.itemId = itemId;
            buying.amount -= newSelling.amount;
            selling.amount -= newSelling.amount;
            prices.create(entityId).copy(price);
        }
        return selling.amount <= 0;
    }

    private boolean finalizeTransaction(String itemId, Iterator<Integer> sellingIds, int buyingId, Price price) {
        Buying buying = buyings.get(buyingId);
        while (sellingIds.hasNext()) {
            Integer sellingId = sellingIds.next();
            Selling selling = sellings.get(sellingId);
            if (buying.amount <= 0) {
                break;
            }
            int entityId = getWorld().create();
            FinalizedTransaction transaction = transactions.create(entityId);
            transaction.id = generateTransactionId();
            Offer sellerOffer = offers.get(sellingId);
            transaction.sellingOfferId = sellerOffer.id;
            transaction.sellerId = sellerOffer.userId;
            Offer buyerOffer = offers.get(buyingId);
            transaction.buyingOfferId = buyerOffer.id;
            transaction.buyerId = buyerOffer.userId;
            Buying newBuying = buyings.create(entityId);
            newBuying.amount = Math.min(buying.amount, selling.amount);
            newBuying.itemId = itemId;
            selling.amount -= newBuying.amount;
            buying.amount -= newBuying.amount;
            prices.create(entityId).copy(price);
        }
        return buying.amount <= 0;
    }

    private static class Market {

        private final TreeMap<Double, List<Integer>> sellingEntityIds;
        private final TreeMap<Double, List<Integer>> buyingEntityIds;

        public Market() {
            sellingEntityIds = new TreeMap<>();
            buyingEntityIds = new TreeMap<>();
        }

    }

}
