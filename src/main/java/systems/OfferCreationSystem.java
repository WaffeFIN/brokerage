package systems;

import com.artemis.Archetype;
import com.artemis.ArchetypeBuilder;
import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.systems.IteratingSystem;
import components.Buying;
import components.User;
import components.Price;
import components.Renderable;
import components.Selling;
import components.Offer;
import components.TimedDeletion;
import components.TraderPrivilege;
import static ids.Util.generateOfferId;
import interfaces.TransactionInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Retrieves and enacts transaction requests
 *
 * @author Walter Grönholm
 */
public class OfferCreationSystem extends IteratingSystem implements TransactionInterface {

    private static final Logger LOG = LoggerFactory.getLogger(OfferCreationSystem.class);
    private static final long OFFER_TIMEOUT_MS = 2 * 60 * 1000;

    private ComponentMapper<Buying> buyings;
    private ComponentMapper<Selling> sellings;
    private ComponentMapper<Price> prices;
    private ComponentMapper<Offer> offers;
    private ComponentMapper<TimedDeletion> deletions;
    private ComponentMapper<User> users;

    private Archetype buyingArchetype;
    private Archetype sellingArchetype;
    private Archetype tradingArchetype;

    public OfferCreationSystem() {
        super(Aspect.all(User.class, TraderPrivilege.class));
    }

    @Override
    protected void initialize() {
        Archetype transactionArchetype = new ArchetypeBuilder()
            .add(Offer.class)
            .add(Renderable.class)
            .add(TimedDeletion.class)
            .build(getWorld());
        buyingArchetype = new ArchetypeBuilder(transactionArchetype)
            .add(Buying.class)
            .add(Price.class)
            .build(getWorld());
        sellingArchetype = new ArchetypeBuilder(transactionArchetype)
            .add(Selling.class)
            .add(Price.class)
            .build(getWorld());
        tradingArchetype = new ArchetypeBuilder(transactionArchetype)
            .add(Buying.class)
            .add(Selling.class)
            .build(getWorld());
    }

    @Override
    protected void process(int i) {
        User controlledUser = users.get(i);
        if (controlledUser.actor != null) {
            controlledUser.actor.actOn(this);
        }
    }

    private Offer initOffer(String userId, int i) {
        String id = generateOfferId();
        Offer offer = offers.get(i);
        offer.id = id;
        offer.userId = userId;
        return offer;
    }

    private long getDeletionTimer(String token) {
        return System.currentTimeMillis() + OFFER_TIMEOUT_MS;
    }

    @Override
    public Offer submitOffer(String token, Buying buying, Price price) {
        int i = getWorld().create(buyingArchetype);
        buyings.get(i).copy(buying);
        prices.get(i).copy(price);
        deletions.get(i).deletionTime = getDeletionTimer(token);
        return initOffer(token, i);
    }

    @Override
    public Offer submitOffer(String token, Selling selling, Price price) {
        int i = getWorld().create(sellingArchetype);
        sellings.get(i).copy(selling);
        prices.get(i).copy(price);
        deletions.get(i).deletionTime = getDeletionTimer(token);
        return initOffer(token, i);
    }

    @Override
    public Offer submitOffer(String token, Buying buying, Selling selling) {
        int i = getWorld().create(tradingArchetype);
        buyings.get(i).copy(buying);
        sellings.get(i).copy(selling);
        deletions.get(i).deletionTime = getDeletionTimer(token);
        return initOffer(token, i);
    }
}
