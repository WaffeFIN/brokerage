package systems;

import com.artemis.Archetype;
import com.artemis.ArchetypeBuilder;
import com.artemis.Aspect;
import com.artemis.BaseEntitySystem;
import com.artemis.ComponentMapper;
import components.User;
import components.LoginRequest;
import components.Name;
import components.TraderPrivilege;
import components.Renderable;
import data.Usergroup;
import data.access.Users;
import actors.StandardInputActor;
import components.AdminPrivilege;
import components.AnalystPrivilege;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Walter Grönholm
 */
public class LoginRequestSystem extends BaseEntitySystem {

    private static final Logger LOG = LoggerFactory.getLogger(LoginRequestSystem.class);

    private ComponentMapper<LoginRequest> requests;
    private ComponentMapper<Name> names;
    private ComponentMapper<User> users;
    private ComponentMapper<AdminPrivilege> adminPrivileges;
    private ComponentMapper<AnalystPrivilege> analystPrivileges;
    private ComponentMapper<TraderPrivilege> traderPrivileges;
    private Archetype userArchetype;

    public LoginRequestSystem() {
        super(Aspect.all(LoginRequest.class));
    }

    @Override
    protected void initialize() {
        userArchetype = new ArchetypeBuilder()
            .add(Name.class)
            .add(User.class)
            .add(Renderable.class)
            .build(getWorld()); //demo, selitä, assessment
    }

    @Override
    protected void inserted(int requestI) {
        LoginRequest request = requests.get(requestI);
        String userName = request.userName;
        if (Users.verifyLogin(userName, request.password)) {
            LOG.info(userName + " logged in");
            int userI = getWorld().create(userArchetype);
            names.get(userI).name = userName;
            String token = generateUserToken(userName);
            User user = users.get(userI);
            user.actor = new StandardInputActor(token);
            handleUserPermissions(userName, userI);
        }
        getWorld().delete(requestI);
    }

    private void handleUserPermissions(String userName, int userI) {
        Usergroup group = Users.getGroup(userName);
        switch(group) {
            case ADMIN:
                adminPrivileges.create(userI);
                analystPrivileges.create(userI);
                traderPrivileges.create(userI);
                break;
            case ANALYST:
                analystPrivileges.create(userI);
                break;
            case TRADER:
                traderPrivileges.create(userI);
                break;
        }
    }

    @Override
    protected void processSystem() {
    }

    @Override
    protected boolean checkProcessing() {
        return false;
    }

    private String generateUserToken(String userName) {
        return UUID.randomUUID().toString(); //TODO REPLACE ME
    }

}
