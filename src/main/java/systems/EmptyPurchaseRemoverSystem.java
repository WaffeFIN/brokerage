package systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.systems.IteratingSystem;
import components.Buying;
import components.FinalizedTransaction;
import components.Selling;

/**
 * Removes empty Buyings and Sellings
 *
 * @author Walter Grönholm
 */
public class EmptyPurchaseRemoverSystem extends IteratingSystem {

    private ComponentMapper<Buying> buyings;
    private ComponentMapper<Selling> sellings;

    public EmptyPurchaseRemoverSystem() {
        super(Aspect.one(Buying.class, Selling.class).exclude(FinalizedTransaction.class));
    }

    @Override
    protected void process(int i) {
        if (buyings.has(i) && buyings.get(i).amount == 0) {
            getWorld().delete(i);
        }
        if (sellings.has(i) && sellings.get(i).amount == 0) {
            getWorld().delete(i);
        }
    }

}
