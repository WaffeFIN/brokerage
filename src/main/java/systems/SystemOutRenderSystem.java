package systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.systems.IteratingSystem;
import components.Buying;
import components.Name;
import components.Price;
import components.Renderable;
import components.Selling;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Walter Grönholm
 */
public class SystemOutRenderSystem extends IteratingSystem {

    private static final Logger LOG = LoggerFactory.getLogger(SystemOutRenderSystem.class);

    private ComponentMapper<Name> names;
    private ComponentMapper<Buying> buyings;
    private ComponentMapper<Selling> sellings;
    private ComponentMapper<Price> prices;

    public SystemOutRenderSystem() {
        super(Aspect.all(Renderable.class));
    }

    @Override
    protected void process(int i) {
        StringBuilder sb = new StringBuilder();
        if (names.has(i)) {
            sb.append(names.get(i).name);
        }
        if (prices.has(i)) {
            sb.append(prices.get(i).value)
                .append(", ");
        }
        if (sellings.has(i)) {
            Selling selling = sellings.get(i);
            sb.append("selling ")
                .append(selling.amount)
                .append("x ")
                .append(selling.itemId)
                .append(", ");
        }
        if (buyings.has(i)) {
            Buying buying = buyings.get(i);
            sb.append("buying ")
                .append(buying.amount)
                .append("x ")
                .append(buying.itemId)
                .append(", ");
        }
        LOG.info(sb.toString());
    }

}
