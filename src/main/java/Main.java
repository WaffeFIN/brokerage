
import runners.ArtemisODBRunner;
import runners.strategies.BrokerageStrategy;

public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new ArtemisODBRunner(new BrokerageStrategy(), 0.2).run();
    }
}
