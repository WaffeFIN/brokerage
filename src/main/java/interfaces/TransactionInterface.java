package interfaces;

import components.Buying;
import components.Price;
import components.Selling;
import components.Offer;

/**
 *
 * @author Walter Grönholm
 */
public interface TransactionInterface {

    Offer submitOffer(String token, Buying buying, Price price);

    Offer submitOffer(String token, Selling selling, Price price);

    Offer submitOffer(String token, Buying buying, Selling selling);
}
