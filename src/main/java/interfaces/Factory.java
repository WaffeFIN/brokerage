package interfaces;

/**
 *
 * @author Walter Grönholm
 */
public interface Factory<V> {

    V create();
}
