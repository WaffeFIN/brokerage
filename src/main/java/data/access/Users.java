package data.access;

import data.Usergroup;
import java.util.HashMap;
import java.util.Map;

/**
 * Dummy class
 *
 * @author Walter Grönholm
 */
public class Users {

    private static Map<String, String> accounts = new HashMap<>();

    static {
        accounts.put("walter", "1234");
        accounts.put("someone", "else");
    }

    public static boolean verifyLogin(String user, String pass) {
        return !pass.isEmpty() && accounts.getOrDefault(user, "").equals(pass);
    }

    public static Usergroup getGroup(String userName) {
        return Usergroup.ADMIN;
    }
}
