package data;

/**
 *
 * @author Walter Grönholm
 */
public enum Currency {

    DOLLARS, EURO, POUNDS_STERLING;

    public static Currency parseCurrency(String string) {
        switch (string.toLowerCase()) {
            case "gbp":
            case "pound":
            case "pounds":
            case "£":
                return POUNDS_STERLING;
            case "usd":
            case "dollar":
            case "dollars":
            case "$":
                return DOLLARS;
            case "eur":
            case "euro":
            case "€":
                return EURO;
        }
        throw new IllegalArgumentException(string + " is not a valid currency");
    }

    @Override
    public String toString() {
        switch (this) {
            case DOLLARS:
                return "$";
            case EURO:
                return "€";
            case POUNDS_STERLING:
                return "£";
        }
        return super.toString();
    }

}
