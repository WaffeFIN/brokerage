package data;

/**
 *
 * @author Walter Grönholm
 */
public enum Usergroup {

    TRADER,
    ANALYST,
    ADMIN;
}
