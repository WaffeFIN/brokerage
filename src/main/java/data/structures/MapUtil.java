package data.structures;

import interfaces.Factory;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Walter Grönholm
 */
public class MapUtil {

    public static <K, V> V getOrCreate(Map<K, V> map, K key, Factory<V> factory) {
        if (!map.containsKey(key)) {
            map.put(key, factory.create());
        }
        return map.get(key);
    }

    public static <K, V> void addToMapList(Map<K, List<V>> map, K key, V value) {
        if (map.containsKey(key)) {
            map.get(key).add(value);
        } else {
            List<V> list = new ArrayList<>();
            map.put(key, list);
            list.add(value);
        }
    }

    public static <K, V> void removeFromMapList(Map<K, List<V>> map, K key, V value) {
        List<V> list = map.get(key);
        if (list != null) {
            list.remove(value);
            if (list.isEmpty()) {
                map.remove(key);
            }
        }
    }
}
