package data.structures;

import java.util.Iterator;

/**
 *
 * @author Walter Grönholm
 */
public class DeepIterator<V> implements Iterator<V> {

    private final Iterator<Iterable<V>> higher;
    private Iterator<V> lower;

    public DeepIterator(Iterable<Iterable<V>> higher) {
        this.higher = higher.iterator();
    }

    @Override
    public boolean hasNext() {
        while ((lower == null || !lower.hasNext()) && higher.hasNext()) {
            lower = higher.next().iterator();
        }
        return lower != null && lower.hasNext();
    }

    @Override
    public V next() {
        return lower.next();
    }
}
